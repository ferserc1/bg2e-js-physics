# bg2e-js-physics
## Physics library for bg2 engine

bg2 engine physics is a set of packages that implements physics for bg2 engine. bg2e physics is fully compatible with the bg2e C++ scene files.

## License

bg2e physics is distributed under the MIT license: you can use it for free, for any purpose, in all the universe (and also in other parallel universes, if they exists), with only two conditions: you can't claim that this work is yours (for example, fork this repository and change my name by yours), and you use this library as is (you can't sue me if this software does something wrong). You can see the full license [here](LICENSE.md)

## Ammo.js

bg2 engine physics is built using ammo.js, a direct port of the bullet physics engine using emscripten. I encourage you to check the [ammo.js]()https://github.com/kripken/ammo.js/ (and [emscripten](https://github.com/kripken/emscripten)) projects, they're amazing!