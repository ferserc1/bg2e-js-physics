(function() {
    bg.physics = bg.physics || {};
    bg.physics.version = "1.0.5 - build: cec2b01";
    var script = document.currentScript.src.split('/');
    script.pop();
    bg.physics.scriptLocation = script.join('/');


    bg.physics.ready = function(searchPath) {
        searchPath = searchPath || bg.physics.scriptLocation + '/ammo.js';
        return new Promise((resolve,reject) => {
            function checkLoaded() {
                if (!window.Ammo) {
                    setTimeout(() => checkLoaded(), 50);
                }
                else {
                    Ammo().then(() => resolve());
                }
            }
            bg.utils.requireGlobal(searchPath);
            checkLoaded();
        });
    }

})();
(function() {

    bg.physics = bg.physics || {};

    class SimulationObject {
        constructor() {
            this._impl = null;
        }

        beginSimulation() { console.warning("Using simulation object without beginSimulation implementation"); }
        endSimulation() { console.warning("Using simulation object without endSimulation implementation"); }
        get isSimulationRunning() { return this._impl!=null; }
    }

    bg.physics.SimulationObject = SimulationObject;

})();
(function() {
    bg.physics = bg.physics || {};

    let s_colliderRegistry = {};

    bg.physics.registerCollider = function(componentClass) {
        let result = /function (.+)\(/.exec(componentClass.toString());
		if (!result) {
			result = /class ([a-zA-Z0-9_]+) /.exec(componentClass.toString());
		}
		let funcName = (result && result.length>1) ? result[1] : "";
		
		bg.physics[funcName] = componentClass;
        componentClass.prototype._typeId = funcName;
        
        s_colliderRegistry[funcName] = componentClass;
    };

    class ColliderShape extends bg.physics.SimulationObject {
        static Factory(jsonData, path) {
            return new Promise((resolve) => {
                let Constructor = s_colliderRegistry[jsonData.shape];
                if (Constructor) {
                    let instance = new Constructor();
                    instance.deserialize(jsonData,path)
                        .then(() => {
                            resolve(instance);
                        });
                }
                else {
                    resolve(null);
                }
            });
            
        }

        constructor() {
            super();
        }
        
        clone() {
            return new ColliderShape();
        }

        deserialize(jsonData,path) {
            return Promise.reject(new Error("Collider deserialize() function not implemented"));
        }

        serialize(jsonData,path) {
            return Promise.reject(new Error("Collider serialize() function not implemented"));
        }

        get dirtyGizmo() {
            throw new Error("ColliderShape: get dirtyGizmo() accessor not implemented by child class");
        }

        getGizmoVertexArray() {
            throw new Error("ColliderShape: getGizmoVertexArray() not implemented by child class");
        }
    }

    bg.physics.ColliderShape = ColliderShape;

})();
(function() {
    
    class BoxCollider extends bg.physics.ColliderShape {
        constructor(width,height,depth) {
            super();
            this._width = width;
            this._height = height;
            this._depth = depth;

            this._dirtyGizmo = true;
        }

        clone() {
            return new BoxCollider(this._width,this._height,this._depth);
        }

        get width() { return this._width; }
        get height() { return this._height; }
        get depth() { return this._depth; }
        set width(value) { this._dirtyGizmo = true; this._width = value; }
        set height(value) { this._dirtyGizmo = true; this._height = value; }
        set depth(value) { this._dirtyGizmo = true; this._depth = value; }

        beginSimulation() {
            this._impl = new Ammo.btBoxShape(new Ammo.btVector3(
                this.width / 2, this.height / 2, this.depth / 2
            ));
            return this._impl;
        }

        endSimulation() {
            if (this._impl) {
                Ammo.destroy(this._impl);
                this._impl = null;
            }
        }

        deserialize(jsonData,path) {
            return new Promise((resolve) => {
                this.width = jsonData.size[0];
                this.height = jsonData.size[1];
                this.depth = jsonData.size[2];
                resolve();
            });
        }

        serialize(jsonData,path) {
            return new Promise((resolve) => {
                jsonData.shape = "BoxCollider";
                jsonData.size = [this._width,this._height,this._depth];
                resolve();
            });
        }

        get dirtyGizmo() {
            return this._dirtyGizmo;
        }

        getGizmoVertexArray() {
            this._dirtyGizmo = false;
            let x = this.width / 2;
            let y = this.height / 2;
            let z = this.depth / 2;
            return [
                // back face
                -x,-y,-z, x,-y,-z, x,-y,-z, x,y,-z, x,y,-z, -x,y,-z, -x,y,-z, -x,-y,-z,

                // front face
                -x,-y,z, x,-y,z, x,-y,z, x,y,z, x,y,z, -x,y,z, -x,y,z, -x,-y,z,

                // edges from back to front face
                -x,-y,-z, -x,-y,z,
                x,-y,-z, x,-y,z,
                x,y,-z, x,y,z,
                -x,y,-z, -x,y,-z
            ];
        }
    }

    bg.physics.registerCollider(BoxCollider);
})();
(function() {

    class ConvexHullCollider extends bg.physics.ColliderShape {
        constructor(margin = 0.01) {
            super();
            this._margin = margin;

            this._dirtyGizmo = true;
        }

        get margin() { return this._margin; }
        set margin(m) { this._margin = m; }

        clone() {
            return new ConvexHullCollider(this._margin);
        }

        beginSimulation() {
            this._impl = new Ammo.btConvexHullShape();
            if (this.node && this.node.drawable) {
                let vec = new Ammo.btVector3();
                this.node.drawable.forEach((plist) => {
                    for (let i = 0; i<plist.vertex.length; i+=3) {
                        vec.setValue(plist.vertex[i],plist.vertex[i + 1],plist.vertex[i + 2]);
                        this._impl.addPoint(vec);
                    }
                });
            }
            this._impl.setMargin(this._margin);
            return this._impl;
        }

        endSimulation() {
            if (this._impl) {
                Ammo.destroy(this._impl);
                this._impl = null;
            }
        }

        deserialize(jsonData,path) {
            return new Promise((resolve) => {
                this.margin = jsonData.margin || 0.001;
                resolve();
            })
        }

        serialize(jsonData,path) {
            return new Promise((resolve) => {
                this._dirtyGizmo = true;
                jsonData.shape = "ConvexHullCollider";
                jsonData.margin = this.margin;
                resolve();
            })
        }

        get dirtyGizmo() {
            return this._dirtyGizmo;
        }

        getGizmoVertexArray() {
            this._dirtyGizmo = false;
            let vertex = [];
            let step = 10;


            return vertex;
        }
    }

    bg.physics.registerCollider(ConvexHullCollider);

})();
(function() {

    class RigidBody extends bg.physics.SimulationObject {
        constructor() {
            super();

            this._mass = NaN;
            this._isKinematic = false;
            this._linearFactor = new bg.Vector3(1,1,1);
            this._angularFactor = new bg.Vector3(1,1,1);

            // this values will be updated during simulation
            this._linearVelocity = new bg.Vector3();
            this._angularVelocity = new bg.Vector3();

            this._restoreTransform = null;
        }

        set restoreTransform(trx) { this._restoreTransform = trx; }
        get restoreTransform() { return this._restoreTransform; }

        beginSimulation(engineData) {
            this._impl = engineData;

            if (engineData.node.transform && !this._restoreTransform) {
                this._restoreTransform = new bg.Matrix4(engineData.node.transform.matrix);
            }
        }

        endSimulation() {
            this._impl = null;
        }

        serialize(jsonData) {
            jsonData.mass = this._mass;
            jsonData.linearFactor = this._linearFactor;
            jsonData.angularFactor = this._angularFactor;
            jsonData.isKinematic = this._isKinematic;
        }

        deserialize(jsonData) {
            this.mass = jsonData.mass;
            this.linearFactor = jsonData.linearFactor;
            this.angularFactor = jsonData.angularFactor;
            this.isKinematic = jsonData.isKinematic;
        }

        get mass() { return this._mass; }
        set mass(m) { this._mass = m; }
        get isKinematic() { return this._isKinematic; }
        set isKinematic(k) { this._isKinematic = k; }

        get linearVelocity() { return this._linearVelocity; }
        set linearVelocity(vel) {
            // TODO: Implement
        }

        get angularVelocity() { return this._angularVelocity; }
        set angularVelocity(vel) {
            // TODO: implement this
        }

        get linearFactor() { return this._linearFactor; }
        set linearFactor(f) {
            // TODO: implement
        }
        get angularFactor() { return this._angularFactor; }
        set angularFactor(f) {
            // TODO: implement
        }

        applyForce(force, relativePos) {
            // TODO: implement
        }

        applyTorque(torque) {
            // TODO: implement
        }

        applyImpulse(impulse, relativePos) {
            // TODO: implement
        }

        applyCentralForce(force) {
            // TODO: implement
        }

        applyTorqueImpulse(torque) {
            // TODO: implement
        }

        applyCentralImpulse(impulse) {
            // TODO: implement
        }

        setTranform(trx) {
            // TODO: implement
        }

        addLinearVelocity(vel) {
            // TODO: implement
        }

        addAngularVelocity(vel) {
            // TODO: implement
        }


    }

    bg.physics.RigidBody = RigidBody;

})();
(function() {

    class SphereCollider extends bg.physics.ColliderShape {
        constructor(radius) {
            super();
            this._radius = radius;

            this._dirtyGizmo = true;
        }

        clone() { return new SphereCollider(this._radius); }

        get radius() { return this._radius; }
        set radius(r) { this._dirtyGizmo = true; this._radius = r; }

        beginSimulation() {
            let pos = new Ammo.btVector3(0,0,0);
            this._impl = new Ammo.btSphereShape(this._radius);
            return this._impl;
        }

        endSimulation() {
            if (this._impl) {
                Ammo.destroy(this._impl);
                this._impl = null;
            }
        }

        deserialize(jsonData,path) {
            return new Promise((resolve) => {
                this.radius = jsonData.radius;
                resolve();
            });
        }

        serialize(jsonData,path) {
            return new Promise((resolve) => {
                this._dirtyGizmo = true;
                jsonData.shape = "SphereCollider";
                jsonData.radius = this.radius;
                resolve();
            });
        }

        get dirtyGizmo() {
            return this._dirtyGizmo;
        }

        getGizmoVertexArray() {
            this._dirtyGizmo = false;
            let vertex = [];
            let step = 10;

            for (let i=0; i<360; i+=step) {
                vertex.push(Math.cos(bg.Math.degreesToRadians(i)) * this.radius);
                vertex.push(Math.sin(bg.Math.degreesToRadians(i)) * this.radius);
                vertex.push(0);

                vertex.push(Math.cos(bg.Math.degreesToRadians(i + step)) * this.radius);
                vertex.push(Math.sin(bg.Math.degreesToRadians(i + step)) * this.radius);
                vertex.push(0);
            }

            for (let i=0; i<360; i+=step) {
                vertex.push(0);
                vertex.push(Math.cos(bg.Math.degreesToRadians(i)) * this.radius);
                vertex.push(Math.sin(bg.Math.degreesToRadians(i)) * this.radius);

                vertex.push(0);
                vertex.push(Math.cos(bg.Math.degreesToRadians(i + step)) * this.radius);
                vertex.push(Math.sin(bg.Math.degreesToRadians(i + step)) * this.radius);
            }

            return vertex;
        }
    }

    bg.physics.registerCollider(SphereCollider);
})();
(function() {
    bg.physics = bg.physics || {};

    class World extends bg.physics.SimulationObject {
        constructor() {
            super();
            this._gravity = new bg.Vector3();
            this._isRunning = false;
            this._minFramerate = 12;
            this._targetFramerate = 60;

            // This value is set by the bg.scene.Dynamics component
            this._dynamics = null;

            let collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
            let dispatcher = new Ammo.btCollisionDispatcher(collisionConfiguration);
            let overlappingPairCache = new Ammo.btDbvtBroadphase();
            let solver = new Ammo.btSequentialImpulseConstraintSolver();
            let dynamicsWorld = new Ammo.btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
            dynamicsWorld.setGravity(new Ammo.btVector3(this._gravity.x, this._gravity.y, this._gravity.z));

            this._engine = {
                collisionConfiguration: collisionConfiguration,
                dispatcher: dispatcher,
                overlappingPairCache: overlappingPairCache,
                solver: solver,
                dynamicsWorld: dynamicsWorld,
                bodies: []
            }
        }

        destroy() {
            this.endSimulation();

            this._engine.bodies = [];
            Ammo.destroy(this._engine.dynamicsWorld);
            Ammo.destroy(this._engine.solver);
            Ammo.destroy(this._engine.overlappingPairCache);
            Ammo.destroy(this._engine.dispatcher);
            Ammo.destroy(this._engine.collisionConfiguration);
        }

        set minFramerate(fr) { this._minFramerate = fr; }
        get minFramerate() { return this._minFramerate; }
        set targetFramerate(fr) { this._targetFramerate = fr; }
        get targetFramerate() { return this._targetFramerate; }

        get sceneComponent() { return this._dynamics }

        set gravity(g) {
            this._gravity = g;
            this._engine.dynamicsWorld.setGravity(new Ammo.btVector3(g.x, g.y, g.z));
        }
        get gravity() { return this._gravity; }

        get isRunning() { return this._isRunning; }

        beginSimulation(node) {
            let sim = node && node.dynamics;

            if (!this.isRunning && sim && this.sceneComponent) {
                this.sceneComponent.eachShape((node, collider, rigidBody) => {
                    this.addNode(node);
                });
                this._isRunning = true;
            }
        }

        simulationStep(delta) {
            if (this._isRunning && this.sceneComponent) {
                let minT = 1 / this._minFramerate;
                let simT = 1 / this._targetFramerate;
                let steps = Math.ceil(minT/simT);

                let deltaMs = delta / 1000;
                this._engine.dynamicsWorld.stepSimulation(1/60,10);
                let btTrx = new Ammo.btTransform();
                this._engine.bodies.forEach((bodyData) => {
                    // bodyData: { btBody, node }
                    bodyData.btBody.getMotionState().getWorldTransform(btTrx);
                    let origin = btTrx.getOrigin();
                    let rotation = btTrx.getRotation();
                    if (bodyData.node.transform) {
                        bodyData.node.transform.matrix.identity();
                        bodyData.node.transform.matrix.translate(origin.x(),origin.y(),origin.z());
                        let axis = rotation.getAxis();
                        bodyData.node.transform.matrix.rotate(rotation.getAngle(),axis.x(), axis.y(),axis.z());
                    }
                });
            }
        }

        endSimulation() {
            if (this._isRunning) {
                
                // TODO: Remove all bodies from the list
                let b = [];
                this._engine.bodies.forEach((bodyData) => b.push(bodyData));
                b.forEach((bodyData) => this.removeNode(bodyData.node));

                this._isRunning = false;
            }
        }

        // Use the following functions to add or remove node to the world
        // during the simulation loop
        removeNode(node) {
            let i = -1;
            this._engine.bodies.some((bodyData,index) => {
                if (bodyData.node==node) {
                    this._engine.dynamicsWorld.removeRigidBody(bodyData.btBody);
                    node.collider.shape.endSimulation();
                    if (node.rigidBody) {
                        node.rigidBody.body.endSimulation();
                    }
                    i = index;
                    return true;
                }
            });

            if (i!=-1) {
                this._engine.bodies.splice(i,1);
            }
        }

        // You can use this function during simulation. When the simulation starts, all the
        // children of this node that contains a collision will be added automatically
        // Do not add nodes that not belong to the dynamics node
        addNode(node) {
            let parent = node.parent;
            let collider = node.collider;
            let transform = node.transform;
            let rigidBodyComponent = node.rigidBody;
            let dynamics = parent && parent.dynamics;
            
            if (this.sceneComponent==dynamics && collider) {
                // Common: transform
                let btTrx = new Ammo.btTransform();
                btTrx.setIdentity();
                if (transform) {
                    btTrx.setFromOpenGLMatrix(transform.matrix.toArray());
                }

                // Shape
                let shape = collider.shape.beginSimulation();

                // Body
                let mass = rigidBodyComponent && rigidBodyComponent.body.mass || 0;
                let btBody = null;
                let localInertia = new Ammo.btVector3(0,0,0);
                let motionState = new Ammo.btDefaultMotionState(btTrx);
                if (mass!=0) {
                    shape.calculateLocalInertia(mass, localInertia);
                }
                btBody = new Ammo.btRigidBody(new Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia));
                
                if (rigidBodyComponent) {
                    rigidBodyComponent.body.beginSimulation({
                        dynamics: this._engine.dynamicsWorld,
                        body: btBody,
                        node: node
                    });
                }

                this._engine.dynamicsWorld.addRigidBody(btBody);
                this._engine.bodies.push({
                    btBody: btBody,
                    node: node
                });                
            }
        }

        rayTest(ray) {
            let result;
            let start = new Ammo.btVector3(ray.start.x, ray.start.y, ray.start.z);
            let end = new Ammo.btVector3(ray.end.x, ray.end.y, ray.end.z);
            /*
            TODO: Implement this
            btCollisionWorld::ClosestRayResultCallback rayCallback(start,end);
            
            bt::world(this)->rayTest(start, end, rayCallback);
            if (rayCallback.hasHit()) {
                const btCollisionObject * obj = rayCallback.m_collisionObject;
                btVector3 point = rayCallback.m_hitPointWorld;
                btVector3 normal = rayCallback.m_hitNormalWorld;
                size_t bodyKey = reinterpret_cast<size_t>(btRigidBody::upcast(obj));
                bg::scene::Node * node = _colliderMap[bodyKey].getPtr();
                result = new bg::physics::RayCastResult(node,
                                                        bg::math::Vector3(point.x(), point.y(), point.z()),
                                                        bg::math::Vector3(normal.x(), normal.y(), normal.z()));
            }
            
            return result.release();
            */
            return null;
        }

        serialize(jsonData) {
            jsonData.gravity = this._gravity.toArray();
        }

        deserialize(jsonData) {
            this.gravity = new bg.Vector3(jsonData.gravity || [0,0,0]);
        }
    }

    bg.physics.World = World;


})();
(function() {
    bg.scene = bg.scene || {};

    function buildPlist(context,vertex,color) {
        let plist = new bg.base.PolyList(context);

        let normal = [];
        let texCoord0 = [];
        let index = [];
        let currentIndex = 0;
        for (let i=0; i<vertex.length; i+=3) {
            normal.push(0); normal.push(0); normal.push(1);
            texCoord0.push(0); texCoord0.push(0);
            index.push(currentIndex++);
        }

        plist.vertex = vertex;
        plist.color = color;
        plist.normal = normal;
        plist.texCoord0 = texCoord0;
        plist.index = index;
        plist.drawMode = bg.base.DrawMode.LINES;
        plist.build();
        return plist;
    }

    function getGizmo() {
        if (!this._gizmo && this.shape) {
            let c = new bg.Color(0.4,0.5,1,1);
            let vertex = this.shape.getGizmoVertexArray();
            let color = [];
            vertex.forEach((v) => {
                color.push(c.r);
                color.push(c.b);
                color.push(c.b);
                color.push(c.a);
            })
            this._gizmo = buildPlist(this.node.context,vertex,color);
        }
        return this._gizmo;
    }



    class Collider extends bg.scene.Component {
        constructor(shape) {
            super();
            this._shape = shape;
        }

        get shape() { return this._shape; }

        clone() {
            let c = new Collider((this._shape && this._shape.clone()) || null);
            return c;
        }

        frame(delta) {
            if (!this._shape.node) {
                this._shape.node = this.node;
            }
        }

        displayGizmo(pipeline,matrixState) {
            if (this.shape) {
                if (this.shape.dirtyGizmo) {
                    this._gizmo = null;
                }
                let plist = getGizmo.apply(this);
                if (plist) {
                    pipeline.draw(plist);
                }
            }
        }

        serialize(componentData,promises,url) {
            if (!bg.isElectronApp) {
                return;
            }
            super.serialize(componentData,promises,url);
            if (this._shape) {
                promises.push(new Promise((resolve,reject) => {
                    this._shape.serialize(componentData,url)
                        .then(() => resolve())
                        .catch((err) => reject(err));
                }));
            }
        }

        deserialize(context,sceneData,url) {
            return new Promise((resolve,reject) => {
                bg.physics.ColliderShape.Factory(sceneData,url)
                    .then((shapeInstance) => {
                        this._shape = shapeInstance;
                        resolve();
                    })
            })
        }
    }

    bg.scene.registerComponent(bg.scene,Collider,"bg.scene.Collider");

    // Add collider function to SceneObject and Component prototypes
    Object.defineProperty(bg.scene.SceneObject.prototype,"collider", { get: function() { return this.component("bg.scene.Collider"); }});
    Object.defineProperty(bg.scene.Component.prototype,"collider", { get: function() { return this.component("bg.scene.Collider"); }});

})();
(function() {

    class Dynamics extends bg.scene.Component {
        constructor(world) {
            super();
            this._world = world || new bg.physics.World();
            this._simulationState = Dynamics.SimulationState.STOPPED;

            if (this._world) {
                this._world._dynamics = this;
            }
        }

        clone() {
            return new Dynamics((this._world && this._world.clone()) || null);
        }

        get world() { return this._world; }
        
        frame(delta) {
            if (this._simulationState == Dynamics.SimulationState.PLAYING) {
                this._world.simulationStep(delta);
            }
        }

        eachShape(fn) {
            if (this.node) {
                this.node.children.forEach((n) => {
                    let collider = n.collider;
                    if (collider) {
                        fn(n, collider, n.rigidBody);
                    }
                })
            }
        }

        play() {
            if (this._simulationState == Dynamics.SimulationState.PAUSED) {
                this._simulationState = Dynamics.SimulationState.PLAYING;
            }
            else if (this._simulationState == Dynamics.SimulationState.STOPPED) {
                this._simulationState = Dynamics.SimulationState.PLAYING;
                this._world.beginSimulation(this.node);
            }
        }

        pause() {
            if (this._simulationState == Dynamics.SimulationState.PLAYING) {
                this._simulationState = Dynamics.SimulationState.PAUSED;
            }
        }

        stop() {
            if (this._simulationState != Dynamics.SimulationState.STOPPED) {
                this._simulationState = Dynamics.SimulationState.STOPPED;
                this._world.endSimulation();
            }
        }

        restore() {
            this.eachShape((node,collider,rigidBody) => {
                if (rigidBody) {
                    rigidBody.restore();
                }
            });
        }

        commit() {
            this.eachShape((node,collider,rigidBody) => {
                if (rigidBody) {
                    rigidBody.commit();
                }
            });
        }

        get simulationState() {
            return this._simulationState;
        }

        serialize(componentData,promises,url) {
            if (!bg.isElectronApp) {
                return;
            }
            super.serialize(componentData,promises,url);
            if (this._world) {
                this._world.serialize(componentData);
            }
            else {
                componentData.gravity = [0,0,0];
            }
        }

        deserialize(context,sceneData,url) {
            if (!this._world) {
                this._world = new bg.physics.World();
                this._world._dynamics = this;
            }
            this._world.deserialize(sceneData);
        }
    }

    Dynamics.SimulationState = {
        STOPPED: 0,
        PLAYING: 1,
        PAUSED: 2
    }

    bg.scene.registerComponent(bg.scene,Dynamics,"bg.scene.Dynamics");

    // Add dynamics to SceneObject and Component prototypes
    Object.defineProperty(bg.scene.SceneObject.prototype,"dynamics", { get: function() { return this.component("bg.scene.Dynamics"); }});
    Object.defineProperty(bg.scene.Component.prototype,"dynamics", { get: function() { return this.component("bg.scene.Dynamics"); }});

    class DynamicsVisitor extends bg.scene.NodeVisitor {
        constructor() {
            super();
            this._result = [];
        }

        reset() { this._result = []; }

        get result() { return this._result; }

        play() {
            this._result.forEach((dyn) => {
                dyn.play();
            });
        }

        pause() {
            this._result.forEach((dyn) => {
                dyn.pause();
            });
        }

        stop() {
            this._result.forEach((dyn) => {
                dyn.stop();
            });
        }

        visit(node) {
            if (node.dynamics) {
                this._result.push(node.dynamics);
            }
        }
    }

    bg.scene.DynamicsVisitor = DynamicsVisitor;

    bg.scene.Node.prototype.findDynamics = function() {
        let visitor = new DynamicsVisitor();
        this.sceneRoot.accept(visitor);
        return visitor.result;
    };

})();
(function() {

    class RigidBody extends bg.scene.Component {
        constructor(rigidBody) {
            super();
            this._rigidBody = rigidBody || new bg.physics.RigidBody();
        }

        clone() {
            return new RigidBody(this._rigidBody.clone());
        }

        get body() { return this._rigidBody; }

        serialize(componentData,promises,url) {
            if (!bg.isElectronApp) {
                return;
            }
            super.serialize(componentData,promises,url);
            this._rigidBody.serialize(componentData);
        }

        deserialize(context,sceneData,url) {
            return new Promise((resolve,reject) => {
                this._rigidBody.deserialize(sceneData);
                resolve();
            });
        }

        // When the simulation is stopped, the following function restores the object
        // transform
        restore() {
            if (!this.body.isSimulationRunning && this.transform && this.body.restoreTransform) {
                this.transform.matrix
                    .identity()
                    .mult(this.body.restoreTransform);
            }
        }

        // When the simulation is stopped, the following function consolidate the 
        // changes in the transform node
        commit() {
            if (!this.body.isSimulationRunning && this.transform) {
                this.body.restoreTransform = null;
            }
        }
    }

    bg.scene.registerComponent(bg.scene,RigidBody,"bg.scene.RigidBody");

    // Add rigidBody to SceneObject and Component prototypes
    Object.defineProperty(bg.scene.SceneObject.prototype,"rigidBody", { get: function() { return this.component("bg.scene.RigidBody"); }});
    Object.defineProperty(bg.scene.Component.prototype,"rigidBody", { get: function() { return this.component("bg.scene.RigidBody"); }});

})();