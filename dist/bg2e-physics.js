"use strict";
(function() {
  bg.physics = bg.physics || {};
  bg.physics.version = "1.0.10 - build: be03b2c";
  var script = document.currentScript.src.split('/');
  script.pop();
  bg.physics.scriptLocation = script.join('/');
  bg.physics.ready = function(searchPath) {
    searchPath = searchPath || bg.physics.scriptLocation + '/ammo.js';
    return new Promise(function(resolve, reject) {
      function checkLoaded() {
        if (!window.Ammo) {
          setTimeout(function() {
            return checkLoaded();
          }, 50);
        } else {
          Ammo().then(function() {
            return resolve();
          });
        }
      }
      bg.utils.requireGlobal(searchPath);
      checkLoaded();
    });
  };
})();

"use strict";
(function() {
  bg.physics = bg.physics || {};
  var SimulationObject = function() {
    function SimulationObject() {
      this._impl = null;
    }
    return ($traceurRuntime.createClass)(SimulationObject, {
      beginSimulation: function() {
        console.warning("Using simulation object without beginSimulation implementation");
      },
      endSimulation: function() {
        console.warning("Using simulation object without endSimulation implementation");
      },
      get isSimulationRunning() {
        return this._impl != null;
      }
    }, {});
  }();
  bg.physics.SimulationObject = SimulationObject;
})();

"use strict";
(function() {
  bg.physics = bg.physics || {};
  var s_colliderRegistry = {};
  bg.physics.registerCollider = function(componentClass) {
    var result = /function (.+)\(/.exec(componentClass.toString());
    if (!result) {
      result = /class ([a-zA-Z0-9_]+) /.exec(componentClass.toString());
    }
    var funcName = (result && result.length > 1) ? result[1] : "";
    bg.physics[funcName] = componentClass;
    componentClass.prototype._typeId = funcName;
    s_colliderRegistry[funcName] = componentClass;
  };
  var ColliderShape = function($__super) {
    function ColliderShape() {
      $traceurRuntime.superConstructor(ColliderShape).call(this);
    }
    return ($traceurRuntime.createClass)(ColliderShape, {
      clone: function() {
        return new ColliderShape();
      },
      deserialize: function(jsonData, path) {
        return Promise.reject(new Error("Collider deserialize() function not implemented"));
      },
      serialize: function(jsonData, path) {
        return Promise.reject(new Error("Collider serialize() function not implemented"));
      },
      get dirtyGizmo() {
        throw new Error("ColliderShape: get dirtyGizmo() accessor not implemented by child class");
      },
      getGizmoVertexArray: function() {
        throw new Error("ColliderShape: getGizmoVertexArray() not implemented by child class");
      }
    }, {Factory: function(jsonData, path) {
        return new Promise(function(resolve) {
          var Constructor = s_colliderRegistry[jsonData.shape];
          if (Constructor) {
            var instance = new Constructor();
            instance.deserialize(jsonData, path).then(function() {
              resolve(instance);
            });
          } else {
            resolve(null);
          }
        });
      }}, $__super);
  }(bg.physics.SimulationObject);
  bg.physics.ColliderShape = ColliderShape;
})();

"use strict";
(function() {
  var BoxCollider = function($__super) {
    function BoxCollider(width, height, depth) {
      $traceurRuntime.superConstructor(BoxCollider).call(this);
      this._width = width;
      this._height = height;
      this._depth = depth;
      this._dirtyGizmo = true;
    }
    return ($traceurRuntime.createClass)(BoxCollider, {
      clone: function() {
        return new BoxCollider(this._width, this._height, this._depth);
      },
      get width() {
        return this._width;
      },
      get height() {
        return this._height;
      },
      get depth() {
        return this._depth;
      },
      set width(value) {
        this._dirtyGizmo = true;
        this._width = value;
      },
      set height(value) {
        this._dirtyGizmo = true;
        this._height = value;
      },
      set depth(value) {
        this._dirtyGizmo = true;
        this._depth = value;
      },
      beginSimulation: function() {
        this._impl = new Ammo.btBoxShape(new Ammo.btVector3(this.width / 2, this.height / 2, this.depth / 2));
        return this._impl;
      },
      endSimulation: function() {
        if (this._impl) {
          Ammo.destroy(this._impl);
          this._impl = null;
        }
      },
      deserialize: function(jsonData, path) {
        var $__1 = this;
        return new Promise(function(resolve) {
          $__1.width = jsonData.size[0];
          $__1.height = jsonData.size[1];
          $__1.depth = jsonData.size[2];
          resolve();
        });
      },
      serialize: function(jsonData, path) {
        var $__1 = this;
        return new Promise(function(resolve) {
          jsonData.shape = "BoxCollider";
          jsonData.size = [$__1._width, $__1._height, $__1._depth];
          resolve();
        });
      },
      get dirtyGizmo() {
        return this._dirtyGizmo;
      },
      getGizmoVertexArray: function() {
        this._dirtyGizmo = false;
        var x = this.width / 2;
        var y = this.height / 2;
        var z = this.depth / 2;
        return [-x, -y, -z, x, -y, -z, x, -y, -z, x, y, -z, x, y, -z, -x, y, -z, -x, y, -z, -x, -y, -z, -x, -y, z, x, -y, z, x, -y, z, x, y, z, x, y, z, -x, y, z, -x, y, z, -x, -y, z, -x, -y, -z, -x, -y, z, x, -y, -z, x, -y, z, x, y, -z, x, y, z, -x, y, -z, -x, y, z];
      }
    }, {}, $__super);
  }(bg.physics.ColliderShape);
  bg.physics.registerCollider(BoxCollider);
})();

"use strict";
(function() {
  var ConvexHullCollider = function($__super) {
    function ConvexHullCollider() {
      var margin = arguments[0] !== (void 0) ? arguments[0] : 0.01;
      $traceurRuntime.superConstructor(ConvexHullCollider).call(this);
      this._margin = margin;
      this._vertexData = null;
      this._vertexDataRaw = null;
      this._dirtyGizmo = true;
    }
    return ($traceurRuntime.createClass)(ConvexHullCollider, {
      get margin() {
        return this._margin;
      },
      set margin(m) {
        this._margin = m;
      },
      get vertexData() {
        return this._vertexDataRaw;
      },
      set vertexData(data) {
        this._setVertexData(data);
      },
      clearVertexData: function() {
        this._vertexData = null;
        this._vertexDataRaw = null;
        this._dirtyGizmo = true;
      },
      setVertexData: function(data) {
        var append = arguments[1] !== (void 0) ? arguments[1] : false;
        var $__1 = this;
        this._dirtyGizmo = true;
        if (!append) {
          this._vertexData = [];
          this._vertexDataRaw = null;
        }
        this._vertexData = this._vertexData || [];
        this._vertexDataRaw = this._vertexDataRaw || [];
        if (data instanceof bg.scene.Drawable) {
          data.forEach(function(plist) {
            return $__1.setVertexData(plist);
          });
        } else if (data instanceof bg.base.PolyList) {
          this.setVertexData(data.vertex);
        } else if (Array.isArray(data)) {
          this._vertexDataRaw = data;
          for (var i = 0; i < data.length; i += 3) {
            this._vertexData.push(new Ammo.btVector3(data[i], data[i + 1], data[i + 2]));
          }
        }
      },
      rebuildVertexData: function() {
        if (this.node && this.node.drawable) {
          this.setVertexData(this.node.drawable);
        }
      },
      clone: function() {
        return new ConvexHullCollider(this._margin);
      },
      beginSimulation: function() {
        var $__1 = this;
        this._impl = new Ammo.btConvexHullShape();
        if (!this._vertexData || this._vertexData.length == 0) {
          this.rebuildVertexData();
        }
        this._vertexData.forEach(function(vert) {
          return $__1._impl.addPoint(vert);
        });
        this._impl.setMargin(this._margin);
        return this._impl;
      },
      endSimulation: function() {
        if (this._impl) {
          Ammo.destroy(this._impl);
          this._impl = null;
        }
      },
      destroy: function() {
        if (this._vertexData) {
          this._vertexData.forEach(function(vec) {
            return Ammo.destroy(vec);
          });
          this._vertexData = [];
        }
      },
      deserialize: function(jsonData, path) {
        var $__1 = this;
        return new Promise(function(resolve) {
          $__1.margin = jsonData.margin || 0.001;
          $__1._vertexData = [];
          if (jsonData.vertexData && jsonData.vertexData.length) {
            for (var i = 0; i < jsonData.vertexData.length; i += 3) {
              $__1._vertexData.push(new Ammo.btVector3(jsonData.vertexData[i], jsonData.vertexData[i + 1], jsonData.vertexData[i + 2]));
            }
          }
          resolve();
        });
      },
      serialize: function(jsonData, path) {
        var $__1 = this;
        return new Promise(function(resolve) {
          $__1._dirtyGizmo = true;
          jsonData.shape = "ConvexHullCollider";
          jsonData.margin = $__1.margin;
          if ($__1._vertexData) {
            jsonData.vertexData = [];
            $__1._vertexData.forEach(function(vert) {
              jsonData.vertexData.push(vert.x(), vert.y(), vert.z());
            });
          }
          resolve();
        });
      },
      get dirtyGizmo() {
        return this._dirtyGizmo;
      },
      getGizmoVertexArray: function() {
        var $__1 = this;
        if (!this._vertexData || this._vertexData.length == 0) {
          this.rebuildVertexData();
        }
        var vertexList = [];
        if (this._vertexData && this._vertexData.length) {
          this._vertexData.forEach(function(vert, index) {
            if (index < $__1._vertexData.length - 1) {
              var next = $__1._vertexData[index + 1];
              vertexList.push(vert.x(), vert.y(), vert.z());
              vertexList.push(next.x(), next.y(), next.z());
            }
          });
          this._dirtyGizmo = false;
        }
        return vertexList;
      }
    }, {}, $__super);
  }(bg.physics.ColliderShape);
  bg.physics.registerCollider(ConvexHullCollider);
})();

"use strict";
(function() {
  var RigidBody = function($__super) {
    function RigidBody() {
      $traceurRuntime.superConstructor(RigidBody).call(this);
      this._mass = NaN;
      this._isKinematic = false;
      this._linearFactor = new bg.Vector3(1, 1, 1);
      this._angularFactor = new bg.Vector3(1, 1, 1);
      this._linearVelocity = new bg.Vector3();
      this._angularVelocity = new bg.Vector3();
      this._restoreTransform = null;
    }
    return ($traceurRuntime.createClass)(RigidBody, {
      set restoreTransform(trx) {
        this._restoreTransform = trx;
      },
      get restoreTransform() {
        return this._restoreTransform;
      },
      clone: function() {
        var result = new RigidBody();
        result._mass = this._mass;
        result._isKinematic = this._isKinematic;
        result._linearFactor = new bg.Vector3(this._linearFactor);
        result._angularFactor = new bg.Vector3(this._angularFactor);
        return result;
      },
      beginSimulation: function(engineData) {
        this._impl = engineData;
        if (engineData.node.transform) {
          this._restoreTransform = new bg.Matrix4(engineData.node.transform.matrix);
        }
      },
      endSimulation: function() {
        this._impl = null;
      },
      serialize: function(jsonData) {
        jsonData.mass = this._mass;
        jsonData.linearFactor = this._linearFactor;
        jsonData.angularFactor = this._angularFactor;
        jsonData.isKinematic = this._isKinematic;
      },
      deserialize: function(jsonData) {
        this.mass = jsonData.mass;
        this.linearFactor = jsonData.linearFactor;
        this.angularFactor = jsonData.angularFactor;
        this.isKinematic = jsonData.isKinematic;
      },
      get mass() {
        return this._mass;
      },
      set mass(m) {
        this._mass = m;
      },
      get isKinematic() {
        return this._isKinematic;
      },
      set isKinematic(k) {
        this._isKinematic = k;
      },
      get linearVelocity() {
        return this._linearVelocity;
      },
      set linearVelocity(vel) {},
      get angularVelocity() {
        return this._angularVelocity;
      },
      set angularVelocity(vel) {},
      get linearFactor() {
        return this._linearFactor;
      },
      set linearFactor(f) {},
      get angularFactor() {
        return this._angularFactor;
      },
      set angularFactor(f) {},
      applyForce: function(force, relativePos) {},
      applyTorque: function(torque) {},
      applyImpulse: function(impulse, relativePos) {},
      applyCentralForce: function(force) {},
      applyTorqueImpulse: function(torque) {},
      applyCentralImpulse: function(impulse) {},
      setTranform: function(trx) {},
      addLinearVelocity: function(vel) {},
      addAngularVelocity: function(vel) {}
    }, {}, $__super);
  }(bg.physics.SimulationObject);
  bg.physics.RigidBody = RigidBody;
})();

"use strict";
(function() {
  var SphereCollider = function($__super) {
    function SphereCollider(radius) {
      $traceurRuntime.superConstructor(SphereCollider).call(this);
      this._radius = radius;
      this._dirtyGizmo = true;
    }
    return ($traceurRuntime.createClass)(SphereCollider, {
      clone: function() {
        return new SphereCollider(this._radius);
      },
      get radius() {
        return this._radius;
      },
      set radius(r) {
        this._dirtyGizmo = true;
        this._radius = r;
      },
      beginSimulation: function() {
        var pos = new Ammo.btVector3(0, 0, 0);
        this._impl = new Ammo.btSphereShape(this._radius);
        return this._impl;
      },
      endSimulation: function() {
        if (this._impl) {
          Ammo.destroy(this._impl);
          this._impl = null;
        }
      },
      deserialize: function(jsonData, path) {
        var $__1 = this;
        return new Promise(function(resolve) {
          $__1.radius = jsonData.radius;
          resolve();
        });
      },
      serialize: function(jsonData, path) {
        var $__1 = this;
        return new Promise(function(resolve) {
          $__1._dirtyGizmo = true;
          jsonData.shape = "SphereCollider";
          jsonData.radius = $__1.radius;
          resolve();
        });
      },
      get dirtyGizmo() {
        return this._dirtyGizmo;
      },
      getGizmoVertexArray: function() {
        this._dirtyGizmo = false;
        var vertex = [];
        var step = 10;
        for (var i = 0; i < 360; i += step) {
          vertex.push(Math.cos(bg.Math.degreesToRadians(i)) * this.radius);
          vertex.push(Math.sin(bg.Math.degreesToRadians(i)) * this.radius);
          vertex.push(0);
          vertex.push(Math.cos(bg.Math.degreesToRadians(i + step)) * this.radius);
          vertex.push(Math.sin(bg.Math.degreesToRadians(i + step)) * this.radius);
          vertex.push(0);
        }
        for (var i$__2 = 0; i$__2 < 360; i$__2 += step) {
          vertex.push(0);
          vertex.push(Math.cos(bg.Math.degreesToRadians(i$__2)) * this.radius);
          vertex.push(Math.sin(bg.Math.degreesToRadians(i$__2)) * this.radius);
          vertex.push(0);
          vertex.push(Math.cos(bg.Math.degreesToRadians(i$__2 + step)) * this.radius);
          vertex.push(Math.sin(bg.Math.degreesToRadians(i$__2 + step)) * this.radius);
        }
        return vertex;
      }
    }, {}, $__super);
  }(bg.physics.ColliderShape);
  bg.physics.registerCollider(SphereCollider);
})();

"use strict";
(function() {
  bg.physics = bg.physics || {};
  var World = function($__super) {
    function World() {
      $traceurRuntime.superConstructor(World).call(this);
      this._gravity = new bg.Vector3();
      this._isRunning = false;
      this._minFramerate = 12;
      this._targetFramerate = 60;
      this._dynamics = null;
      var collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
      var dispatcher = new Ammo.btCollisionDispatcher(collisionConfiguration);
      var overlappingPairCache = new Ammo.btDbvtBroadphase();
      var solver = new Ammo.btSequentialImpulseConstraintSolver();
      var dynamicsWorld = new Ammo.btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
      dynamicsWorld.setGravity(new Ammo.btVector3(this._gravity.x, this._gravity.y, this._gravity.z));
      this._engine = {
        collisionConfiguration: collisionConfiguration,
        dispatcher: dispatcher,
        overlappingPairCache: overlappingPairCache,
        solver: solver,
        dynamicsWorld: dynamicsWorld,
        bodies: []
      };
    }
    return ($traceurRuntime.createClass)(World, {
      clone: function() {
        var result = new World();
        result.gravity = new bg.Vector3(this._gravity);
        return result;
      },
      destroy: function() {
        this.endSimulation();
        this._engine.bodies = [];
        Ammo.destroy(this._engine.dynamicsWorld);
        Ammo.destroy(this._engine.solver);
        Ammo.destroy(this._engine.overlappingPairCache);
        Ammo.destroy(this._engine.dispatcher);
        Ammo.destroy(this._engine.collisionConfiguration);
      },
      set minFramerate(fr) {
        this._minFramerate = fr;
      },
      get minFramerate() {
        return this._minFramerate;
      },
      set targetFramerate(fr) {
        this._targetFramerate = fr;
      },
      get targetFramerate() {
        return this._targetFramerate;
      },
      get sceneComponent() {
        return this._dynamics;
      },
      set gravity(g) {
        this._gravity = g;
        this._engine.dynamicsWorld.setGravity(new Ammo.btVector3(g.x, g.y, g.z));
      },
      get gravity() {
        return this._gravity;
      },
      get isRunning() {
        return this._isRunning;
      },
      beginSimulation: function(node) {
        var $__2 = this;
        var sim = node && node.dynamics;
        if (!this.isRunning && sim && this.sceneComponent) {
          this.sceneComponent.eachShape(function(node, collider, rigidBody) {
            $__2.addNode(node);
          });
          this._isRunning = true;
        }
      },
      simulationStep: function(delta) {
        if (this._isRunning && this.sceneComponent) {
          var minT = 1 / this._minFramerate;
          var simT = 1 / this._targetFramerate;
          var steps = Math.ceil(minT / simT);
          var deltaMs = delta / 1000;
          this._engine.dynamicsWorld.stepSimulation(1 / 60, 10);
          var btTrx = new Ammo.btTransform();
          this._engine.bodies.forEach(function(bodyData) {
            bodyData.btBody.getMotionState().getWorldTransform(btTrx);
            var origin = btTrx.getOrigin();
            var rotation = btTrx.getRotation();
            if (bodyData.node.transform) {
              bodyData.node.transform.matrix.identity();
              bodyData.node.transform.matrix.translate(origin.x(), origin.y(), origin.z());
              var axis = rotation.getAxis();
              bodyData.node.transform.matrix.rotate(rotation.getAngle(), axis.x(), axis.y(), axis.z());
            }
          });
        }
      },
      endSimulation: function() {
        var $__2 = this;
        if (this._isRunning) {
          var b = [];
          this._engine.bodies.forEach(function(bodyData) {
            return b.push(bodyData);
          });
          b.forEach(function(bodyData) {
            return $__2.removeNode(bodyData.node);
          });
          this._isRunning = false;
        }
      },
      removeNode: function(node) {
        var $__2 = this;
        var i = -1;
        this._engine.bodies.some(function(bodyData, index) {
          if (bodyData.node == node) {
            $__2._engine.dynamicsWorld.removeRigidBody(bodyData.btBody);
            node.collider.shape.endSimulation();
            if (node.rigidBody) {
              node.rigidBody.body.endSimulation();
            }
            i = index;
            return true;
          }
        });
        if (i != -1) {
          this._engine.bodies.splice(i, 1);
        }
      },
      addNode: function(node) {
        var parent = node.parent;
        var collider = node.collider;
        var transform = node.transform;
        var rigidBodyComponent = node.rigidBody;
        var dynamics = parent && parent.dynamics;
        if (this.sceneComponent == dynamics && collider) {
          var btTrx = new Ammo.btTransform();
          btTrx.setIdentity();
          if (transform) {
            btTrx.setFromOpenGLMatrix(transform.matrix.toArray());
          }
          var shape = collider.shape.beginSimulation();
          var mass = rigidBodyComponent && rigidBodyComponent.body.mass || 0;
          var btBody = null;
          var localInertia = new Ammo.btVector3(0, 0, 0);
          var motionState = new Ammo.btDefaultMotionState(btTrx);
          if (mass != 0) {
            shape.calculateLocalInertia(mass, localInertia);
          }
          btBody = new Ammo.btRigidBody(new Ammo.btRigidBodyConstructionInfo(mass, motionState, shape, localInertia));
          if (rigidBodyComponent) {
            rigidBodyComponent.body.beginSimulation({
              dynamics: this._engine.dynamicsWorld,
              body: btBody,
              node: node
            });
          }
          this._engine.dynamicsWorld.addRigidBody(btBody);
          this._engine.bodies.push({
            btBody: btBody,
            node: node
          });
        }
      },
      rayTest: function(ray) {
        var result;
        var start = new Ammo.btVector3(ray.start.x, ray.start.y, ray.start.z);
        var end = new Ammo.btVector3(ray.end.x, ray.end.y, ray.end.z);
        return null;
      },
      serialize: function(jsonData) {
        jsonData.gravity = this._gravity.toArray();
      },
      deserialize: function(jsonData) {
        this.gravity = new bg.Vector3(jsonData.gravity || [0, 0, 0]);
      }
    }, {}, $__super);
  }(bg.physics.SimulationObject);
  bg.physics.World = World;
})();

"use strict";
(function() {
  bg.scene = bg.scene || {};
  function buildPlist(context, vertex, color) {
    var plist = new bg.base.PolyList(context);
    var normal = [];
    var texCoord0 = [];
    var index = [];
    var currentIndex = 0;
    for (var i = 0; i < vertex.length; i += 3) {
      normal.push(0);
      normal.push(0);
      normal.push(1);
      texCoord0.push(0);
      texCoord0.push(0);
      index.push(currentIndex++);
    }
    plist.vertex = vertex;
    plist.color = color;
    plist.normal = normal;
    plist.texCoord0 = texCoord0;
    plist.index = index;
    plist.drawMode = bg.base.DrawMode.LINES;
    plist.build();
    return plist;
  }
  function getGizmo() {
    if (!this._gizmo && this.shape) {
      var c = new bg.Color(0.4, 0.5, 1, 1);
      var vertex = this.shape.getGizmoVertexArray();
      var color = [];
      vertex.forEach(function(v) {
        color.push(c.r);
        color.push(c.b);
        color.push(c.b);
        color.push(c.a);
      });
      this._gizmo = buildPlist(this.node.context, vertex, color);
    }
    return this._gizmo;
  }
  var Collider = function($__super) {
    function Collider(shape) {
      $traceurRuntime.superConstructor(Collider).call(this);
      this._shape = shape;
    }
    return ($traceurRuntime.createClass)(Collider, {
      get shape() {
        return this._shape;
      },
      clone: function() {
        var c = new Collider((this._shape && this._shape.clone()) || null);
        return c;
      },
      frame: function(delta) {
        if (!this._shape.node) {
          this._shape.node = this.node;
        }
      },
      displayGizmo: function(pipeline, matrixState) {
        if (this.shape) {
          if (this.shape.dirtyGizmo) {
            this._gizmo = null;
          }
          var plist = getGizmo.apply(this);
          if (plist) {
            pipeline.draw(plist);
          }
        }
      },
      serialize: function(componentData, promises, url) {
        var $__2 = this;
        if (!bg.isElectronApp) {
          return;
        }
        $traceurRuntime.superGet(this, Collider.prototype, "serialize").call(this, componentData, promises, url);
        if (this._shape) {
          promises.push(new Promise(function(resolve, reject) {
            $__2._shape.serialize(componentData, url).then(function() {
              return resolve();
            }).catch(function(err) {
              return reject(err);
            });
          }));
        }
      },
      deserialize: function(context, sceneData, url) {
        var $__2 = this;
        return new Promise(function(resolve, reject) {
          bg.physics.ColliderShape.Factory(sceneData, url).then(function(shapeInstance) {
            $__2._shape = shapeInstance;
            resolve();
          });
        });
      }
    }, {}, $__super);
  }(bg.scene.Component);
  bg.scene.registerComponent(bg.scene, Collider, "bg.scene.Collider");
  Object.defineProperty(bg.scene.SceneObject.prototype, "collider", {get: function() {
      return this.component("bg.scene.Collider");
    }});
  Object.defineProperty(bg.scene.Component.prototype, "collider", {get: function() {
      return this.component("bg.scene.Collider");
    }});
})();

"use strict";
(function() {
  var Dynamics = function($__super) {
    function Dynamics(world) {
      $traceurRuntime.superConstructor(Dynamics).call(this);
      this._world = world || new bg.physics.World();
      this._simulationState = Dynamics.SimulationState.STOPPED;
      if (this._world) {
        this._world._dynamics = this;
      }
    }
    return ($traceurRuntime.createClass)(Dynamics, {
      clone: function() {
        return new Dynamics((this._world && this._world.clone()) || null);
      },
      get world() {
        return this._world;
      },
      frame: function(delta) {
        if (this._simulationState == Dynamics.SimulationState.PLAYING) {
          this._world.simulationStep(delta);
        }
      },
      eachShape: function(fn) {
        if (this.node) {
          this.node.children.forEach(function(n) {
            var collider = n.collider;
            if (collider) {
              fn(n, collider, n.rigidBody);
            }
          });
        }
      },
      play: function() {
        if (this._simulationState == Dynamics.SimulationState.PAUSED) {
          this._simulationState = Dynamics.SimulationState.PLAYING;
        } else if (this._simulationState == Dynamics.SimulationState.STOPPED) {
          this._simulationState = Dynamics.SimulationState.PLAYING;
          this._world.beginSimulation(this.node);
        }
      },
      pause: function() {
        if (this._simulationState == Dynamics.SimulationState.PLAYING) {
          this._simulationState = Dynamics.SimulationState.PAUSED;
        }
      },
      stop: function() {
        if (this._simulationState != Dynamics.SimulationState.STOPPED) {
          this._simulationState = Dynamics.SimulationState.STOPPED;
          this._world.endSimulation();
        }
      },
      restore: function() {
        this.eachShape(function(node, collider, rigidBody) {
          if (rigidBody) {
            rigidBody.restore();
          }
        });
      },
      commit: function() {
        this.eachShape(function(node, collider, rigidBody) {
          if (rigidBody) {
            rigidBody.commit();
          }
        });
      },
      get simulationState() {
        return this._simulationState;
      },
      serialize: function(componentData, promises, url) {
        if (!bg.isElectronApp) {
          return;
        }
        $traceurRuntime.superGet(this, Dynamics.prototype, "serialize").call(this, componentData, promises, url);
        if (this._world) {
          this._world.serialize(componentData);
        } else {
          componentData.gravity = [0, 0, 0];
        }
      },
      deserialize: function(context, sceneData, url) {
        if (!this._world) {
          this._world = new bg.physics.World();
          this._world._dynamics = this;
        }
        this._world.deserialize(sceneData);
      }
    }, {}, $__super);
  }(bg.scene.Component);
  Dynamics.SimulationState = {
    STOPPED: 0,
    PLAYING: 1,
    PAUSED: 2
  };
  bg.scene.registerComponent(bg.scene, Dynamics, "bg.scene.Dynamics");
  Object.defineProperty(bg.scene.SceneObject.prototype, "dynamics", {get: function() {
      return this.component("bg.scene.Dynamics");
    }});
  Object.defineProperty(bg.scene.Component.prototype, "dynamics", {get: function() {
      return this.component("bg.scene.Dynamics");
    }});
  var DynamicsVisitor = function($__super) {
    function DynamicsVisitor() {
      $traceurRuntime.superConstructor(DynamicsVisitor).call(this);
      this._result = [];
    }
    return ($traceurRuntime.createClass)(DynamicsVisitor, {
      reset: function() {
        this._result = [];
      },
      get result() {
        return this._result;
      },
      play: function() {
        this._result.forEach(function(dyn) {
          dyn.play();
        });
      },
      pause: function() {
        this._result.forEach(function(dyn) {
          dyn.pause();
        });
      },
      stop: function() {
        this._result.forEach(function(dyn) {
          dyn.stop();
        });
      },
      visit: function(node) {
        if (node.dynamics) {
          this._result.push(node.dynamics);
        }
      }
    }, {}, $__super);
  }(bg.scene.NodeVisitor);
  bg.scene.DynamicsVisitor = DynamicsVisitor;
  bg.scene.Node.prototype.findDynamics = function() {
    var visitor = new DynamicsVisitor();
    this.sceneRoot.accept(visitor);
    return visitor.result;
  };
})();

"use strict";
(function() {
  var RigidBody = function($__super) {
    function RigidBody(rigidBody) {
      $traceurRuntime.superConstructor(RigidBody).call(this);
      this._rigidBody = rigidBody || new bg.physics.RigidBody();
    }
    return ($traceurRuntime.createClass)(RigidBody, {
      clone: function() {
        return new RigidBody(this._rigidBody.clone());
      },
      get body() {
        return this._rigidBody;
      },
      serialize: function(componentData, promises, url) {
        if (!bg.isElectronApp) {
          return;
        }
        $traceurRuntime.superGet(this, RigidBody.prototype, "serialize").call(this, componentData, promises, url);
        this._rigidBody.serialize(componentData);
      },
      deserialize: function(context, sceneData, url) {
        var $__2 = this;
        return new Promise(function(resolve, reject) {
          $__2._rigidBody.deserialize(sceneData);
          resolve();
        });
      },
      restore: function() {
        if (!this.body.isSimulationRunning && this.transform && this.body.restoreTransform) {
          this.transform.matrix.identity().mult(this.body.restoreTransform);
        }
      },
      commit: function() {
        if (!this.body.isSimulationRunning && this.transform) {
          this.body.restoreTransform = null;
        }
      }
    }, {}, $__super);
  }(bg.scene.Component);
  bg.scene.registerComponent(bg.scene, RigidBody, "bg.scene.RigidBody");
  Object.defineProperty(bg.scene.SceneObject.prototype, "rigidBody", {get: function() {
      return this.component("bg.scene.RigidBody");
    }});
  Object.defineProperty(bg.scene.Component.prototype, "rigidBody", {get: function() {
      return this.component("bg.scene.RigidBody");
    }});
})();
