
class PhysicsWindowController extends bg.app.WindowController {
    play() {
        if (this._scene) {
            this._scene.findDynamics().forEach((dyn) => {
                dyn.play();
            });
        }
    }

    pause() {
        if (this._scene) {
            this._scene.findDynamics().forEach((dyn) => {
                dyn.pause();
            })
        }
    }

    stop() {
        if (this._scene) {
            this._scene.findDynamics().forEach((dyn) => {
                dyn.stop();
            })
        }
    }

    restore() {
        if (this._scene) {
            this._scene.findDynamics().forEach((dyn) => {
                dyn.stop();
                dyn.restore();
            })
        }
    }

    scene1() {
        bg.base.Loader.Load(this.gl,"../data/test/scene.vitscnj")
        .then((result) => {
            this._scene = result.sceneRoot;
            let cameraNode = result.cameraNode;
            this._camera = cameraNode.camera;

            // Post reshape (to update the camera viewport)
            this.postReshape();
        });
    }

    scene2() {
        this._scene = new bg.scene.Node(this.gl,"Scene root");

        let lightNode = new bg.scene.Node(this.gl,"Main light");
        let l = new bg.base.Light(this.gl);
        l.type = bg.base.LightType.DIRECTIONAL;
        lightNode.addComponent(new bg.scene.Light(l));
        lightNode.addComponent(new bg.scene.Transform(
            bg.Matrix4.Identity()
                .rotate(bg.Math.degreesToRadians(30),0,1,0)
                .rotate(bg.Math.degreesToRadians(65),-1,0,0)
        ));
        this._scene.addChild(lightNode);

        let world = new bg.scene.Node(this.gl,"World");
        let dynamics = new bg.scene.Dynamics();
        dynamics.world.gravity = new bg.Vector3(0,-10,0);
        world.addComponent(dynamics);
        this._scene.addChild(world);

        let ball = new bg.scene.Node(this.gl,"Ball");
        ball.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,4,0)));
        ball.addComponent(bg.scene.PrimitiveFactory.Sphere(this.gl,0.5));
        ball.addComponent(new bg.scene.RigidBody());
        ball.addComponent(new bg.scene.Collider(new bg.physics.SphereCollider(0.5)));
        ball.rigidBody.body.mass = 2;
        world.addChild(ball);

        let floor = new bg.scene.Node(this.gl,"Floor");
        floor.addComponent(new bg.scene.Transform(bg.Matrix4.Translation(0,-0.5,0).rotate(0.2,0,0,1).rotate(0.1,-1,0,0)));
        floor.addComponent(bg.scene.PrimitiveFactory.Cube(this.gl,10,0.1,10));
        floor.addComponent(new bg.scene.Collider(new bg.physics.BoxCollider(10,0.1,10)));
        world.addChild(floor);

        this._camera = new bg.scene.Camera();
        this._camera.projectionStrategy = new bg.scene.OpticalProjectionStrategy();
        this._camera.projectionStrategy.focalLength = 80;
        let cameraNode = new bg.scene.Node("Camera");
        cameraNode.addComponent(this._camera);
        cameraNode.addComponent(new bg.scene.Transform());
        let ctrl = new bg.manipulation.OrbitCameraController();
        ctrl.minPitch = -90;
        cameraNode.addComponent(ctrl);
        this._scene.addChild(cameraNode);
    }
    
    init() {
        bg.Engine.Set(new bg.webgl1.Engine(this.gl));

        bg.base.Loader.RegisterPlugin(new bg.base.TextureLoaderPlugin());
		bg.base.Loader.RegisterPlugin(new bg.base.VWGLBLoaderPlugin());
        bg.base.Loader.RegisterPlugin(new bg.base.SceneLoaderPlugin());
        
        bg.physics.ready().then(() =>{
            this.scene1();

            // Post reshape (to update the camera viewport)
            this.postReshape();
        });
                
        this._renderer = bg.render.Renderer.Create(this.gl,bg.render.RenderPath.FORWARD);
        this._inputVisitor = new bg.scene.InputVisitor();
    }

    frame(delta) {
        this._scene && this._renderer.frame(this._scene, delta);
    }

    display() {
        this._camera && this._scene && this._renderer.display(this._scene, this._camera);
    }

    reshape(w,h) {
        this._camera && (this._camera.viewport = new bg.Viewport(0,0,w,h));
    }

    mouseDown(evt) { this._scene && this._inputVisitor.mouseDown(this._scene,evt); }
    mouseDrag(evt) { this._scene && this._inputVisitor.mouseDrag(this._scene,evt); }
    mouseWheel(evt) { this._scene && this._inputVisitor.mouseWheel(this._scene,evt); }
    touchStart(evt) { this._scene && this._inputVisitor.touchStart(this._scene,evt); }
    touchMove(evt) { this._scene && this._inputVisitor.touchMove(this._scene,evt); }
}

function load() {
    let controller = new PhysicsWindowController();
    let mainLoop = bg.app.MainLoop.singleton;
    window.windowController = controller;
    
    mainLoop.updateMode = bg.app.FrameUpdate.AUTO;
    mainLoop.canvas = document.getElementsByTagName("canvas")[0];
    mainLoop.run(controller);
}
